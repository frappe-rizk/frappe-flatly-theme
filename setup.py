from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in frappe_flatly_theme/__init__.py
from frappe_flatly_theme import __version__ as version

setup(
	name="frappe_flatly_theme",
	version=version,
	description="Theme",
	author="Karim Rizk",
	author_email="karim@rizk.dev",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
